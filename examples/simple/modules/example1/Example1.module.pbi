﻿; /**
;  * This is the Example1 Module of the PureWork examples.
;  * 
;  * @author Marc Sven Kleinböhl (aka Hroudtwolf)
;  * 
;  * This file is part of PureWork.
;  * 
;  * PureWork is free software: you can redistribute it And/Or modify
;  * it under the terms of the GNU General Public License As published by
;  * the Free Software Foundation, either version 3 of the License, Or
;  * (at your option) any later version.
;  * 
;  * PureWork is distributed in the hope that it will be useful,
;  * but WITHOUT ANY WARRANTY; without even the implied warranty of
;  * MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
;  * GNU General Public License For more details.
;  * 
;  * You should have received a copy of the GNU General Public License
;  * along With PureWork.  If Not, see <http://www.gnu.org/licenses/>.
;  */

DeclareModule Example1
  
EndDeclareModule

Module Example1
  
  ; Registers this Module To the PureWork framework.
  PureWork::registerModule("Example1")
  
  ; /**
  ;  * Implements PureWork::onStart() hook.
  ;  */
  Runtime Procedure onStart(*argument.PureWork::tHookStartArgument)
    
    Debug "Example1 Module -> onStart called."
    
    If (*argument\argc > 0)
      Debug "First programm parameter: " + *argument\argv[0]
    EndIf
    
    ProcedureReturn
  EndProcedure  
  
  ; /**
  ;  * Implements PureWork::onExit() hook.
  ;  */
  Runtime Procedure onExit(*argument.PureWork::tHookArgument)
    
    Debug "Example1 Module -> onExit called."
    
    ProcedureReturn
  EndProcedure   
  
  ; /**
  ;  * Implements ANYMODULE::onSomething() hook.
  ;  */
  Runtime Procedure onSomething(*argument.PureWork::tHookArgument)
    
    Debug "Example1 Module -> onSomething called."
    
    ProcedureReturn
  EndProcedure   
EndModule
  