﻿; /**
;  * This is the Example2 Module of the PureWork examples.
;  * 
;  * @author Marc Sven Kleinböhl (aka Hroudtwolf)
;  * 
;  * This file is part of PureWork.
;  * 
;  * PureWork is free software: you can redistribute it And/Or modify
;  * it under the terms of the GNU General Public License As published by
;  * the Free Software Foundation, either version 3 of the License, Or
;  * (at your option) any later version.
;  * 
;  * PureWork is distributed in the hope that it will be useful,
;  * but WITHOUT ANY WARRANTY; without even the implied warranty of
;  * MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
;  * GNU General Public License For more details.
;  * 
;  * You should have received a copy of the GNU General Public License
;  * along With PureWork.  If Not, see <http://www.gnu.org/licenses/>.
;  */


DeclareModule Example2
  
EndDeclareModule

Module Example2
  
  PureWork::registerModule("Example2")
  
  Runtime Procedure onStart(*argument.PureWork::tHookArgument)
    
    Debug "Example2 Module -> onStart called."
    
    ProcedureReturn
  EndProcedure  
  
  Runtime Procedure onSomething(*argument.PureWork::tHookStartArgument)
    
    Debug "Example2 Module -> onSomething called."
    
    ProcedureReturn
  EndProcedure     
EndModule
  