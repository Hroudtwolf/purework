; /**
;  * This is the config module of the PureWork
;  * application framework.
;  *
;  * @author Marc Sven Kleinb�hl (aka Hroudtwolf)
;  * 
;  * This file is part of PureWork.
;  * 
;  * PureWork is free software: you can redistribute it And/Or modify
;  * it under the terms of the GNU General Public License As published by
;  * the Free Software Foundation, either version 3 of the License, Or
;  * (at your option) any later version.
;  * 
;  * PureWork is distributed in the hope that it will be useful,
;  * but WITHOUT ANY WARRANTY; without even the implied warranty of
;  * MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
;  * GNU General Public License For more details.
;  * 
;  * You should have received a copy of the GNU General Public License
;  * along With PureWork.  If Not, see <http://www.gnu.org/licenses/>.
;  */
DeclareModule Configurations

  Prototype.i pConfigurationEnumerator(key.s, value.s)

  ; /**
  ;  * Parameter type for hook onConfigurationChanged.
  ;  */
  Structure tHookConfigurationChanged Extends PureWork::tHookArgument
    key   .s  ; /** The key of the config value that has been changed. */
    value .s  ; /** The value that has been changed. */
  EndStructure

  Declare.s getConfig(key.s, defaultValue.s = "")
  Declare   setConfig(key.s, value.s)
  Declare   enumerateConfigurations(*enumerationCallback.pConfigurationEnumerator)
EndDeclareModule

Module Configurations
  
  ; /**
  ;  * Holds the configuration of an application.
  ;  * 
  ;  * @var map<string> configurations
  ;  */
  Global NewMap configurations.s()
  
  ; /**
  ;  * Implements PureWork::onStart() hook.
  ;  */
  Runtime Procedure onStart(*argument.PureWork::tHookArgument)
 
    PureWork::invokeHook("onLoadingConfiguration", #Null)

    ProcedureReturn
  EndProcedure

  ; /**
  ;  * Returns a configuration value. 
  ;  * 
  ;  * @param   string key          The key of the config value.
  ;  * @param   string defaultValue (Optional) The default value will be returned if the value doesn't exist for the specific key.
  ;  * @return  string              The config value or default value if no value exist for the specific key.
  ;  */
  Procedure.s getConfig(key.s, defaultValue.s = "")

    If (FindMapElement(configurations(), key) = #Null)
      ProcedureReturn defaultValue
    EndIf

    ProcedureReturn configurations(key)
  EndProcedure

  ; /**
  ;  * Sets a configuration value. 
  ;  * 
  ;  * @param   string key          The key of the config value.
  ;  * @param   string value        The config value.
  ;  */
  Procedure setConfig(key.s, value.s)

    Protected.tHookConfigurationChanged argument

    argument\key    = key
    argument\value  = value
    
    PureWork::invokeHook("onConfigurationChanged", argument)

    configurations(key) = argument\value

    ProcedureReturn
  EndProcedure

  ; /**
  ;  * Enumerates all config values and keys.
  ;  * It stops when the callback returns #False.
  ;  * 
  ;  * @param pConfigurationEnumerator  *enumerationCallback    The enumerator callback pointer.
  ;  */
  Procedure enumerateConfigurations(*enumerationCallback.pConfigurationEnumerator)
    
    ForEach(configurations())
      If (Not *enumerationCallback(MapKey(configurations()), configurations()))
        ProcedureReturn
      EndIf
    Next

  EndProcedure
EndModule
