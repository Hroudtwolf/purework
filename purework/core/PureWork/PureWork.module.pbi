﻿; /**
;  * This is the main Module of the PureWork
;  * application framework.
;  *
;  * @author Marc Sven Kleinböhl (aka Hroudtwolf)
;  * 
;  * This file is part of PureWork.
;  * 
;  * PureWork is free software: you can redistribute it And/Or modify
;  * it under the terms of the GNU General Public License As published by
;  * the Free Software Foundation, either version 3 of the License, Or
;  * (at your option) any later version.
;  * 
;  * PureWork is distributed in the hope that it will be useful,
;  * but WITHOUT ANY WARRANTY; without even the implied warranty of
;  * MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
;  * GNU General Public License For more details.
;  * 
;  * You should have received a copy of the GNU General Public License
;  * along With PureWork.  If Not, see <http://www.gnu.org/licenses/>.
;  */
DeclareModule PureWork
  

  ; /**
  ;  * A structure which describes a x/y position.
  ;  */
  Structure tPoint
    x.i
    y.i
  EndStructure
  
  ; /**
  ;  * A structure which describes a size.
  ;  */
  Structure tSize
    width.i
    height.i
  EndStructure

  ;/**
  ; * The main structure for all hook argument types.
  ; */
  Structure tHookArgument
  EndStructure
  
  ;/**
  ; * Contains the number of maximum savable program parameters 
  ; * in tHookStartArgument typed variables.
  ; *
  ; * @var #MAX_ARGC
  ; */
  #MAX_ARGC = 255
  
  ;/**
  ; * The hook argument type For the onStart hook.
  ; *
  ; * @var integer  argc   Contains the amount of available program parameters.
  ; * @var string[] argv   An array that contains the program parameters.
  ; */
  Structure tHookStartArgument Extends tHookArgument
    argc.i
    argv.s[#MAX_ARGC]
  EndStructure
  
  Declare.i registerModule(moduleName.s)
  Declare.i invokeHook(hookName.s, *argument = #Null)
  Declare.i start()
  Declare.i exit()
  Declare.i setModulePriorityHigh(moduleName.s)
  Declare.i setModulePriorityLow(moduleName.s)
EndDeclareModule

Module PureWork
  
  ; /**
  ;  * Holds the registered module names.
  ;  * 
  ;  * @var list<string> modules
  ;  */
  Global NewList modules.s()
  
  ;/** 
  ; * Invoked on application start.
  ; */
  Procedure.i start() 
    
    Protected.tHookStartArgument argument
    Protected.i i
    
    InitializeStructure(argument, tHookStartArgument)
    
    argument\argc       = CountProgramParameters()
    
    While (argument\argc > i And i < #MAX_ARGC) 
      argument\argv[i] = ProgramParameter(i)
      i=i+1
    Wend
    
    invokeHook("onStart", argument)
    
  EndProcedure
  
  ;/**
  ; * Invoked on application exit.
  ; */
  Procedure.i exit() 
    
    invokeHook("onExit")
    
  EndProcedure
  

  ;/** 
  ; * A module can call this function to
  ; * register itself to the framework.
  ; *
  ; * @param string moduleName   The name of the module.
  ; */ 
  Procedure.i registerModule(moduleName.s)
 
    AddElement(modules())
    modules() = moduleName
    
    ProcedureReturn
  EndProcedure

  ;/** 
  ; * A module can call this function to
  ; * register itself to the framework.
  ; *
  ; * @param string moduleName   The name of the module.
  ; */ 
  Procedure.i setMainModule(moduleName.s)
 
    If (Not IsRuntime(moduleName + "::main()"))
      
    EndIf

    setModulePriorityHigh(moduleName)
    start()
    invokeHook("main")
    exit()
    
    ProcedureReturn
  EndProcedure
  
  ;/** 
  ; * Invokes a specific module hook.
  ; *
  ; * @param string        hookName   The name of the hook function.
  ; * @param tHookArgument *argument  The generic hook argument to be bypassed to the hook functions.
  ; */
  Procedure.i invokeHook(hookName.s, *argument.tHookArgument = #Null)
    
    Protected *procAddress
    Protected succceed.i = #True

    ForEach(modules())
       
      *procAddress = GetRuntimeInteger(modules() + "::" + hookName + "()")
      If (*procAddress = #Null)
        Continue
      EndIf
 
      If (*argument = #Null)
        result = CallFunctionFast(*procAddress)
      Else
        result = CallFunctionFast(*procAddress, *argument)
      EndIf
      

      If (Not result)
        succeed = #False
      EndIf
    Next
    
    ProcedureReturn succeed
  EndProcedure

  ;/** 
  ; * Sets the invoke priority of a specific registered module to "high".
  ; *
  ; * @param  string        hookName   The name of the hook function.
  ; * @return boolean       TRUE on success. FALSE if the modules wasn't registered.
  ; */
  Procedure.i setModulePriorityHigh(moduleName.s)
    
    ForEach(modules())
       
      If (modules() = moduleName)
        
        DeleteElement(modules())
        FirstElement(modules())
        InsertElement(modules())
        modules() = moduleName

        ProcedureReturn #True
      EndIf
      
    Next

    ProcedureReturn #False
  EndProcedure

  ;/** 
  ; * Sets the invoke priority of a specific registered module to "low".
  ; *
  ; * @param  string        hookName   The name of the hook function.
  ; * @return boolean       TRUE on success. FALSE if the modules wasn't registered.
  ; */
  Procedure.i setModulePriorityLow(moduleName.s)
    
    ForEach(modules())
       
      If (modules() = moduleName)
        
        DeleteElement(modules())
        LastElement(modules())
        AddElement(modules())
        modules() = moduleName

        ProcedureReturn #True
      EndIf
      
    Next

    ProcedureReturn #False
  EndProcedure
EndModule
