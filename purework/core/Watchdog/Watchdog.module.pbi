﻿; /**
;  * This is the Watchdog Module of the PureWork
;  * application framework.
;  * This module provides an "onWatchdog" hook which you can
;  * implement into your modules to catch error messages.
;  * All onWatchdog invoking will get a tWatchdogArgument argument bypassed.
;  *
;  * @author Marc Sven Kleinböhl (aka Hroudtwolf)
;  * 
;  * This file is part of PureWork.
;  * 
;  * PureWork is free software: you can redistribute it And/Or modify
;  * it under the terms of the GNU General Public License As published by
;  * the Free Software Foundation, either version 3 of the License, Or
;  * (at your option) any later version.
;  * 
;  * PureWork is distributed in the hope that it will be useful,
;  * but WITHOUT ANY WARRANTY; without even the implied warranty of
;  * MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
;  * GNU General Public License For more details.
;  * 
;  * You should have received a copy of the GNU General Public License
;  * along With PureWork.  If Not, see <http://www.gnu.org/licenses/>.
;  */
DeclareModule Watchdog
  
  ; /**
  ;  * @var string #THROW_TYPE_NOTICE   An identifier for watchdog messages of the type "Notice".
  ;  * @var string #THROW_TYPE_ERROR    An identifier For watchdog messages of the type "Error".
  ;  */
  #THROW_TYPE_NOTICE = "Notice"
  #THROW_TYPE_ERROR  = "Error"
  
  Structure tHookWatchdogArgument Extends PureWork::tHookArgument
    message.s           ; /** The error message. */
    file.s              ; /** The file where the error occured. */
    line.i              ; /** The line where the error occured. */
    code.i              ; /** The error code. */
    type.s              ; /** Type of the entry (#THROW_TYPE_...). */
    preventDefault.i    ; /** Set by hooks (#true) if the default output should be prevented. */
  EndStructure

  Declare throw(message.s, type.s = #THROW_TYPE_NOTICE, file.s = "", line.i = 0, errorCode.i = 0)
  
EndDeclareModule

Module Watchdog
  
  ; Registers this Module To the PureWork framework.
  PureWork::registerModule(#PB_Compiler_Module)

  Declare onErrorCallback()

  ; /**
  ;  * The callback invoked by onErrorCall().
  ;  */
  Procedure onErrorCallback()
     
    throw(ErrorMessage(), #THROW_TYPE_ERROR, ErrorFile(), ErrorLine(), ErrorCode())

    ProcedureReturn
  EndProcedure
  
  ; /**
  ;  * Implements PureWork::onStart() hook.
  ;  */
  Runtime Procedure onStart(*argument.PureWork::tHookArgument)
 
    CompilerIf #PB_Compiler_Debugger = #False
      OnErrorCall(@ onErrorCallback())
    CompilerEndIf
    
    ProcedureReturn
  EndProcedure
  
  ; /**
  ;  * Implements PureWork::onExit() hook.
  ;  */
  Runtime Procedure onExit(*argument.PureWork::tHookArgument)
    
    OnErrorDefault()

    ProcedureReturn
  EndProcedure

  ; /**
  ;  * Throws an error or a notice.
  ;  *
  ;  * @param string  message     The message text-
  ;  * @param string  type        (Optional. By default: #THROW_TYPE_NOTICE) Specifies if an error or a notice was thrown.
  ;  * @param string  file        (Optional) The file where the notice or error was thrown.
  ;  * @param integer line        (Optional) The line where the notice or error was thrown.
  ;  * @param integer errorCode   ONLY USED WHEN CALLED BY onErrorCallback.
  ;  */
  Procedure throw(message.s, type.s = #THROW_TYPE_NOTICE, file.s = "", line.i = 0, errorCode.i = 0)
    
    Protected.tHookWatchdogArgument argument
    
    With argument
      \message          = message
      \line             = line
      \code             = errorCode
      \file             = file
      \type             = type
      \preventDefault   = #False
    EndWith
    
    PureWork::invokeHook("onWatchdog", argument)

    If (argument\preventDefault = #False)
      Debug type + " #" + argument\code + " '" + argument\message + "' in '" + argument\file + "' [" + argument\line + "]."
    EndIf

    ProcedureReturn
  EndProcedure
EndModule
