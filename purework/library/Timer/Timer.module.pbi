; /**
;  * This is the HiRes timer module of the PureWork
;  * application framework.
;  *
;  * @author Marc Sven Kleinb�hl (aka Hroudtwolf)
;  * 
;  * This file is part of PureWork.
;  * 
;  * PureWork is free software: you can redistribute it And/Or modify
;  * it under the terms of the GNU General Public License As published by
;  * the Free Software Foundation, either version 3 of the License, Or
;  * (at your option) any later version.
;  * 
;  * PureWork is distributed in the hope that it will be useful,
;  * but WITHOUT ANY WARRANTY; without even the implied warranty of
;  * MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
;  * GNU General Public License For more details.
;  * 
;  * You should have received a copy of the GNU General Public License
;  * along With PureWork.  If Not, see <http://www.gnu.org/licenses/>.
;  */
DeclareModule Timer
  
  Declare.i useTimer(ID.s)
  Declare.i start()
  Declare.i stop()
  Declare.i resume()
  Declare.i destroy()
  Declare.q get()
  Declare.i create(ID.s)
EndDeclareModule

Module Timer


  CompilerIf #PB_Compiler_OS = #PB_OS_Windows
    #WINDOWS_FIX_FACTOR = 0.1
  CompilerEndIf
  
  Structure tTimer
    lastUpdate.q

    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      frequency.q
    CompilerElseIf #PB_Compiler_OS = #PB_OS_Linux      
      *timerObject
    CompilerEndIf
  EndStructure

  Global currentTimer.s
  Global *currentTimer.tTimer
  Global NewMap *timers.tTimer()

  ; /**
  ;  * Selects a timer to be the current timer by ID.
  ;  * 
  ;  * @param string ID     The ID of the timer to select.
  ;  */
  Procedure.i useTimer(ID.s)
    
    If (Not FindMapElement(*timers(), ID))
      ProcedureReturn #False
    EndIf

    *currentTimer = *timers(ID)
    currentTimer  = ID

    ProcedureReturn #True
  EndProcedure

  ; /**
  ;  * Starts the current timer.
  ;  * 
  ;  * @return boolean TRUE on success.
  ;  */
  Procedure.i start()
    
    If (*currentTimer = #Null)
      ProcedureReturn #False
    EndIf

    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      
      QueryPerformanceFrequency_(@*currentTimer\frequency)
      QueryPerformanceCounter_(@*currentTimer\lastUpdate)
      *currentTimer\lastUpdate = *currentTimer\lastUpdate
    CompilerElseIf #PB_Compiler_OS = #PB_OS_Linux
      g_timer_start_(*currentTimer\timerObject)
     *currentTimer\lastUpdate = g_timer_elapsed_(*currentTimer\timerObject, #Null)
    CompilerEndIf

    ProcedureReturn #True
  EndProcedure

  ; /**
  ;  * Stops the current timer.
  ;  * 
  ;  * @return boolean TRUE on success.
  ;  */
  Procedure.i stop()

    If (*currentTimer = #Null)
      ProcedureReturn #False
    EndIf

    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      *currentTimer\lastUpdate = 0
    CompilerElseIf #PB_Compiler_OS = #PB_OS_Linux
      g_timer_stop_(*currentTimer\timerObject)
    CompilerEndIf

    ProcedureReturn #True
  EndProcedure

  ; /**
  ;  * Resumes the current timer if it was previously stopped.
  ;  * 
  ;  * @return boolean TRUE on success.
  ;  */
  Procedure.i resume()

    If (*currentTimer = #Null)
      ProcedureReturn #False
    EndIf

    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      start()
    CompilerElseIf #PB_Compiler_OS = #PB_OS_Linux
      g_timer_continue_(*currentTimer\timerObject)
    CompilerEndIf

    ProcedureReturn #True
  EndProcedure

  ; /**
  ;  * Destroys the current timer.
  ;  */
  Procedure.i destroy()

    DeleteMapElement(*timers(), currentTimer)

    CompilerIf #PB_Compiler_OS = #PB_OS_Linux
      g_timer_destroy_(*currentTimer\timerObject)
    CompilerEndIf

    FreeMemory(*currentTimer)

    *currentTimer = #Null

  EndProcedure

  ; /**
  ;  * Retrieves the elapsed time since the last call or the staring of the current timer.
  ;  * 
  ;  * @return quad   The elapsed time.
  ;  */
  Procedure.q get()

    Protected.q microseconds
    Protected current.q
    
    If (*currentTimer = #Null Or *currentTimer\lastUpdate = 0)
      ProcedureReturn 0
    EndIf

    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      QueryPerformanceCounter_(@*currentTimer\lastUpdate)
      microseconds = *currentTimer\lastUpdate / *currentTimer\frequency
      microseconds * #WINDOWS_FIX_FACTOR 
    CompilerElseIf #PB_Compiler_OS = #PB_OS_Linux
      current = g_timer_elapsed_(*currentTimer\timerObject, #Null)
      microseconds = current - *currentTimer\lastUpdate
      *currentTimer\lastUpdate = current
    CompilerEndIf

    ProcedureReturn microseconds
  EndProcedure
  
  ; /**
  ;  * Creates a timer.
  ;  * 
  ;  * @param string ID     The ID of the new timer.
  ;  */
  Procedure.i create(ID.s)

    *currentTimer = AllocateMemory(SizeOf(tTimer))

    CompilerIf #PB_Compiler_OS = #PB_OS_Linux
      *currentTimer\timerObject = g_timer_new_()
    CompilerEndIf

    *timers(ID)    = *currentTimer

  EndProcedure
EndModule
