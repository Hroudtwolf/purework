﻿; /**
;  * This is the pack file, that includes all necessary framework files.
;  *
;  * @author Marc Sven Kleinböhl (aka Hroudtwolf)
;  * 
;  * This file is part of PureWork.
;  * 
;  * PureWork is free software: you can redistribute it And/Or modify
;  * it under the terms of the GNU General Public License As published by
;  * the Free Software Foundation, either version 3 of the License, Or
;  * (at your option) any later version.
;  * 
;  * PureWork is distributed in the hope that it will be useful,
;  * but WITHOUT ANY WARRANTY; without even the implied warranty of
;  * MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
;  * GNU General Public License For more details.
;  * 
;  * You should have received a copy of the GNU General Public License
;  * along With PureWork.  If Not, see <http://www.gnu.org/licenses/>.
;  */

EnableExplicit

; Includes the main module of the framework.
XIncludeFile "core/PureWork/PureWork.module.pbi"

; Includes the core module Configuration.
XIncludeFile "core/Configuration/Configuration.module.pbi"

; Includes the core module Watchdog.
XIncludeFile "core/Watchdog/Watchdog.module.pbi"

; /**
;  * A helper macro for importing library modules of the PureWork framework.
;  * 
;  * @param string __module_name__    The name of the module without path and file extension.
;  */
Macro importLibraryModule(__module_name__)
  XIncludeFile "library/" + __module_name__ + "/" + __module_name__ + ".module.pbi"
  PureWork::registerModule(__module_name__)
EndMacro
